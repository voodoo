.. _index:

==============================
vudo 
==============================

The vudo project aims to extend :mod:`repoze.bfg`, a `Python
<http://www.python.org>`_ web framework, with functionality tailored
for content-driven applications. See the `framework documentation
<http://static.repoze.org/bfgdocs/>`_ to learn more about the
platform.

The software is developed and released under a `BSD-like license
<http://www.zope.org/Resources/ZPL>`_.

We share Python's philosophy of having one obvious way to do things
which also helps limiting the scope of the project as a whole: a
framework can only take you so far.


Getting started
===============

The best way to get acquainted with the software is to check out and
install the demo application. The following steps shows you how to do
this using the ``buildout`` build system.

Start by cloning the project repository::

  ~ $ git clone http://repo.or.cz/r/voodoo.git vudo

This will pull down the most recent revision of the repository. Now we
can build out the demo application::

  ~ $ cd vudo/demo/weblog
  weblog $ python2.5 bootstrap.py
  weblog $ bin/buildout

To run the demo application, you need to start the application using
:mod:`Paste`::

  weblog $ bin/paster serve debug.ini

Packages
========

The library packages are the main focus of the project; when possible,
packages are self-contained and provide immediate functionality for
the developer.

:mod:`vudo.cmf`
   Provides a minimal content management framework that other packages can extend with specific functionality. Most applications will use this package.


Index
=====

.. toctree::
   :maxdepth: 2

   api/vudo

Support and Development
=======================

Bugs should be reported to the `mailinglist
<http://groups.google.com/group/vudo>`_.

You can browse the `repository <http://repo.or.cz/w/voodoo.git>`_ or
check out a development copy using Git::

  git clone git+ssh://<username>@repo.or.cz/srv/git/voodoo.git

Note that in order to check out the repository you must create a
(free) account at `repo.or.cz <http://repo.or.cz>`_.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

