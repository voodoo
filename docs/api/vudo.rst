.. _vudo_api:

API Reference
=============

.. _:mod:`vudo.cmf`:
.. _vudo_cmf_module:

:mod:`vudo.cmf.form`
--------------------

.. automodule:: vudo.cmf.form

  .. autoclass:: vudo.cmf.form.BaseForm
  .. autoclass:: vudo.cmf.form.RequestForm
  .. autoclass:: vudo.cmf.form.EditForm
  .. autoclass:: vudo.cmf.form.AddForm
  
:mod:`vudo.cmf.interfaces`
--------------------------

.. automodule:: vudo.cmf.interfaces

  .. autoclass:: vudo.cmf.interfaces.IContent
  .. autoclass:: vudo.cmf.interfaces.IContainer

  .. autoclass:: vudo.cmf.interfaces.IContentManager
  .. autoclass:: vudo.cmf.interfaces.IContentMetadata
  
  .. autoclass:: vudo.cmf.interfaces.IModel
  .. autoclass:: vudo.cmf.interfaces.IForm
  .. autoclass:: vudo.cmf.interfaces.IAction

  .. autoclass:: vudo.cmf.interfaces.IObjectMovedEvent
  .. autoclass:: vudo.cmf.interfaces.IObjectAddedEvent
  .. autoclass:: vudo.cmf.interfaces.IObjectRemovedEvent
  .. autoclass:: vudo.cmf.interfaces.IObjectBeforeModifiedEvent
  
:mod:`vudo.cmf.model`
---------------------

.. automodule:: vudo.cmf.model

  .. autoclass:: vudo.cmf.model.ContentEventSupport
  .. autoclass:: vudo.cmf.model.ContainerEventSupport