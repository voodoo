#!/bin/bash
set -x
GIT_BASE=$(pwd)/../..
PACKAGES="cmf"

for p in $PACKAGES; do
    SRC="$GIT_BASE/vudo.$p/src/vudo/$p/skin"
    TGT="skin/$p"
    test -d $SRC || {
        echo "ERROR: Expected the 'skin' directory of $p in \"$SRC\"."
        echo
        echo "Currently it's expected to use the git source check-out of vudo"
        echo "to run this application -- If you've 'develop'ed the relevant packages"
        echo "and intend to edit their skins, please install 'vudo.skinsetup', which"
        echo "installs the 'vudoskin' tool, which allows to manage these links."
        exit
    }
    echo "Symlinking $SRC -> $TGT"
    ln -sf $SRC $TGT
done
