from zope import interface
from zope import component

from repoze.bfg.skins import get_skin_template
from repoze.bfg.skins import render_skin_template_to_response
from repoze.bfg.traversal import model_url

from vudo.cmf.interfaces import IGetRequest
from vudo.cmf.interfaces import IPostRequest

import forms
import models

def index(context, request, **kwargs):
    template = get_skin_template(context, IGetRequest, "index")
    return template(
        context, request, attributes={
        'base': {'href': model_url(context, request)}
        }, **kwargs)

@component.adapter(models.Entry, IPostRequest)
def add_comment(context, request):
    form = forms.CommentForm(context, request)
    form.status = form()
    return index(context, request, form=form)

@component.adapter(models.Blog, IGetRequest)
def rss(context, request):
    return render_skin_template_to_response(
        context, request, "rss")
