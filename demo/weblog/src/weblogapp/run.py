import os

from zope import interface
from zope import component

from zope.configuration.xmlconfig import xmlconfig

from repoze.bfg import router
from repoze.bfg.interfaces import IRequest
from repoze.bfg.interfaces import IView
from repoze.bfg.registry import get_options
from repoze.bfg.registry import registry_manager
from repoze.bfg.wsgi import wsgiapp

from repoze.zodbconn.finder import PersistentApplicationFinder

from repoze.bfg.static import static_view_factory
from repoze.bfg.static import get_url

import paste.urlparser
import models
import weblogapp


def make_app(global_config, dbconf=None, skin_path=None):

    root_name = 'root' # XXX: should this be a configuration option?

    def app_maker(root):
        if not root_name in root:
            root[root_name] = models.Blog()
            import transaction
            transaction.commit()
        return root[root_name]

    app = router.make_app(
        PersistentApplicationFinder(dbconf, app_maker),
        weblogapp, options=get_options(global_config))

    if skin_path is not None:
        # get application registry
        registry_manager.set(app.registry)
        component.getGlobalSiteManager = registry_manager.get

        # load skin configuration
        xmlconfig(file("%s/configure.zcml" % skin_path))

        # register skin directory
        app.registry.registerAdapter(
            static_view_factory(skin_path), name='static')

    return app
