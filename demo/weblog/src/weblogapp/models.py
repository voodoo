from zope import interface
from zope import component

from zope.lifecycleevent.interfaces import IObjectCreatedEvent

from persistent import Persistent
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping

from vudo.cmf.model import ContentEventSupport
from vudo.cmf.model import ContainerEventSupport
from vudo.cmf.interfaces import IContainer
from vudo.cmf.interfaces import IModel
from vudo.cmf.interfaces import IContent

class Blog(ContainerEventSupport, PersistentMapping):
    """A ZODB-persistent blog."""

    interface.implements(IModel, IContainer)

    title = u""
    description = u""

    @property
    def __allowed_types__(self):
        return Entry,

    def get_entries_by_date(self):
        return sorted(
            self.values(),
            key=lambda entry: entry.created,
            reverse=True)

class Entry(ContentEventSupport, Persistent):
    "A ZODB-persistent blog entry."""

    interface.implements(IModel)
    
    __meta__ = {
        'title': u"Blog entry"}

    @property
    def __allowed_types__(self):
        return Comment,
    
    description = u""
    mimetype = 'text/x-web-markdown'
    
    def __init__(self, title=u"", body=u""):
        super(Entry, self).__init__()
        self.title = title
        self.body = body
        self.comments = PersistentList()

    def get_comments_count(self):
        return len(self.comments)

    def get_comments_by_date(self):
        return sorted(
            self.comments,
            key=lambda entry: entry.created)

class Comment(ContentEventSupport, Persistent):
    "A ZODB-persistent blog comment."""

    interface.implements(IContent)
    
    __meta__ = {
        'title': u"Blog comment"}

    def __init__(self, author=u"", text=u""):
        super(Comment, self).__init__()
        self.author = author
        self.text = text

@component.adapter(Blog, IObjectCreatedEvent)
def setup_default_content(blog, event):
    entry = blog["welcome"] = Entry(
        u"Welcome",
        u"This is your first entry! It was created automatically.")

    entry.comments.append(Comment(
        u"Anonymous",
        u"I can't live without this blog. It's like black coffee."))
