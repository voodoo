from zope import interface
from zope import component

from vudo.cmf.form import EditForm
from vudo.cmf.form import RequestForm
from vudo.cmf.interfaces import IForm

from repoze import formapi
from repoze.bfg.interfaces import IRequest

import models

class EntryForm(EditForm):
    component.adapts(models.Entry, IRequest)

    fields = {
        'title': unicode,
        'body': unicode,
        }

class CommentForm(RequestForm):
    component.adapts(models.Entry, IRequest)

    fields = {
        'author': unicode,
        'text': unicode,
        }

    prefix = "add_comment"

    @formapi.action
    def handle_add(self, data):
        author = data['author']
        text = data['text']

        if not author:
            author = "Anonymous"

        if text:
            self.context.comments.append(
                models.Comment(author, text))
            return u"Your comment was added."
