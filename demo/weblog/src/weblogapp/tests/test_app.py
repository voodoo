import unittest
import time
import os

from zope import interface
from zope import component

import zope.component.testing
import zope.configuration.xmlconfig

class ViewTests(unittest.TestCase):
    def setUp(self):
        zope.component.testing.setUp(self)

    def tearDown(self):
        zope.component.testing.tearDown(self)


class DummyRequest:
    application_url = 'http://app'
    def __init__(self, environ=None, params=None):
        if environ is None:
            environ = {}
        self.environ = environ
        if params is None:
            params = {}
        self.params = params

def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite((ViewTests)))
    return suite

# vim: set ft=python ts=4 sw=4 expandtab :
