Weblog demo
===========

This demo application demonstrates how the content management
framework in the ``vudo.cmf`` package can be used to create a web
application for blogging.

It's a WSGI application that runs on Paste.

Getting started
---------------

To run the demo application, you need to:

- Install the application's dependencies using ``zc.buildout``
- Start the application using ``Paste``

This is outlined below.

Running the `buildout`
~~~~~~~~~~~~~~~~~~~~~~

Usually, the following should get you going::

  $ python2.5 bootstrap.py
  (some output)
  $ bin/buildout
  (some output)

The above will create the `bin/paste` script (among other things).
Along the installation process, it will also install a *statically*
built ``lxml`` egg.

You can find out more about ``zc.buildout`` here_.

.. _here: http://pypi.python.org/pypi/zc.buildout

Starting the application
~~~~~~~~~~~~~~~~~~~~~~~~

This application uses a WSGI_ setup, and is started using `paste`, a
script from the Paste_ Python package.  The application is configured
using a *ini*-style configuration in ``debug.ini`` in the package
root.

You may start the application using::

  $ bin/paste serve debug.ini

The application should start and listen on http://localhost:8080 for
requests.  In case you need it, you ma change the default port and
address in the configuration file ``debug.ini``.

.. _WSGI: http://en.wikipedia.org/wiki/WSGI
.. _Paste: http://pypi.python.org/pypi/Paste

Management interface
--------------------

The demo application uses the WebHUD protocol (for which there
currently exists a Safari plugin) to expose a management interface.

Get the plugin from the homepage_.

.. _homepage: http://code.google.com/p/webhud

Alternatively, you may visit the `edit <http://localhost:8080/edit>`_
and `add <http://localhost:8080/add>`_ pages manually.

Skin Customization
------------------

The ``skin`` of this application may be modified without touching the
code.  The skin is located in the `skin` directory in the same
directory as this file is in.

All skin directories and resources need to be registered explicitly
using ZCML.  The top-level ZCML file is read on application startup
time and is located in `skin/configure.zcml`.

HTML templates
~~~~~~~~~~~~~~

**NOTE**
  The terminology here is ambiguous -- we need a better name here.

This Application uses `repoze.bfg.htmlpage_` to define HTML
*templates*.  A CSS-like syntax is used to define *regions* in these
templates, into which dynamic HTML is rendered.  These templates are
located in the `skin/html` directory.  The rules for defining regions
of dynamic content are read from the `.xss` files named like the
templates.  The HTML templates may refer to **any** resource local to
the `skin/html` directory.  Links to these resources are rebased
dynamically.

There are currently two such templates, the ``default`` template, and
the ``webhud`` template.  The former is used mainly for *viewing*, the
latter is used to *create* and *edit* content using the *WebHUD*.

The directory these templates live in is registered in the skin's
top-level ZCML file.

.. _repoze.bfg.htmlpage: http://svn.repoze.org/repoze.bfg.htmlpage/trunk

Framework Templates
~~~~~~~~~~~~~~~~~~~

Unlike in other systems, *vudo* does not automatically supply it's
framework skins to the application.  Skins which are supplied by
*vudo* packages are referred either by **symbolic links** or, if an
application wants to modify them, by **copy**.  *vudo* packages
advertise their skins using *setuptool entrypoints*.  These links may
be managed with the `skinsetup` tool.

The rationale here is that *vudo* wants to encourage developers and
designers to modify skins provided by *vudo* packages and
applications. Thus we want to manage them outside of the source tree
of the actual application code.  Skins may of course be packaged up
and/or deployed however the user wants.

This application is very simple and thus only uses the `vudo.cmf`
skin.  The `buildout` automatically installs a symbolic link to the
`vudo.cmf` package skin directory.

You may list the available skins by issuing::

  $ bin/skinsetup -l
  skin templates available:

  Skin Name, Package name, Skin path
  --------------------------------------------------------------------------------
  cmf, vudo.cmf, /Users/seletz/develop/vudo.git/demo/weblog/eggs/vudo.cmf-0.2-py2.5.egg/vudo/cmf/skin
  total 1 templates.

You may read more about the `skinsetup` tool on it's homepage_.

.. _homepage: http://pypi.python.org/pypi/vudo.skinsetup

Skin Templates
~~~~~~~~~~~~~~

The actual content is rendered using `repoze.bfg.skins_` -- a package
which allows to register file-system page templates.  These skin
templates are located in `skin/templates` and configured in the
`configure.zcml` in there.

**model** directory
  Contains all ``views`` which are directly accessible using HTTP Get
  requests.  They are registered using the `<bfg:templates />`
  directive.

**blog**, **entry** directories
  These directories contain skins specific to the `Blog` and `Entry`
  content models.  These are not directly accessible.  They are
  referred by the application itself (they are more like ``actions``,
  registered for HTTP Post requests).

.. _repoze.bfg.skins: http://svn.repoze.org/repoze.bfg.skins/trunk

Credits
-------

Malthe Borch <mborch@gmail.com>
Stefan Eletzhofer <stefan.eletzhofer@inquant.de>

