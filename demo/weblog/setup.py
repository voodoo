import os
import sys

from ez_setup import use_setuptools
use_setuptools()

from setuptools import setup, find_packages

requires = [
    'setuptools',
    'repoze.bfg',
    'repoze.tm2',
    'repoze.zodbconn',
    'chameleon.html',
    'vudo.cmf',
    'wsgiutils',
    'repoze.bfg.static',
    'repoze.bfg.skins',
    'repoze.bfg.htmlpage',
    ]

if sys.version_info[:3] < (2,5,1):
    print "Please upgrade to Python 2.5.1 or higher."
    sys.exit(1)

setup(name='weblogapp',
      version='0.1',
      author='Malthe Borch and Stefan Eletzhofer',
      author_email="repoze-dev@lists.repoze.org",
      url='http://www.repoze.org',
      license="BSD-derived (http://www.repoze.org/LICENSE.txt)",
      keywords='web wsgi bfg zope',
      package_dir = {'': 'src'},
      packages=find_packages("src"),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points = """\
      [paste.app_factory]
      make_app = weblogapp.run:make_app
      """
      )

