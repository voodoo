Changelog
=========

0.1 (2008-12-08)
----------------

- Script entry point for ``vudoskin`` script. [seletz]

- Very basic API for vudo packages which want to provide a skin [seletz]

- Initial package setup. [seletz]
