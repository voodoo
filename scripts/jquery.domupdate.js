/* -------------------------------------------------------------------------

jquery.domupdate.js
version 0.2 (git/unreleased)

Dependencies:

* md5.js

Dual licensed under the MIT and GPL licenses.
----------------------------------------------------------------------------
Copyright (C) 2008 Malthe Borch <mborch@gmail.com>
----------------------------------------------------------------------------
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

------------------------------------------------------------------------- */ 

(function($) {
  function outerHTML(element) {
    return $('<div>').append($(element).eq(0).clone()).html();
  }
  
  $.fn.domUpdate = function(html) {
    // this function merges the HTML contents into the root tree in
    // such a way that nodes that are unchanged are left alone

    var index = {};
    var siblings = this.siblings();
    
    // calculate md5 hash values of siblings
    $.each(siblings, function(i, j) {
        var html = outerHTML(j);
        var hash = hex_md5(html);
        
        if (index[hash]) {
          index[hash].push(j);
        } else {
          index[hash] = [j];
        }
      });
    
    var children = $("<div>"+html+"</div>").children();
    
    $.each(children, function(i, j) {
        var html = outerHTML(j);
        var hash = hex_md5(html);
        var elements = index[hash];
        
        sibling = (elements) ? elements.shift() : null;
        
        if (sibling) {
          // element already exists
          $.each(siblings, function(k, l) {
              if (l == sibling) {
                // remove all previous siblings
                siblings.slice(0, k).remove();
                
                // update siblings array
                siblings = siblings.slice(k+1);
              }
            });
        } else {
          // element doesn't exist -- insert
          if (siblings.length > 0) {
            $(j).insertBefore(siblings.eq(0));
          } else {
            this.parent().append(j);
          }
        }
      });
    
    // remove remaining siblings
    $.each(siblings, function(i, j) {
        $(j).remove();
      });
  };
 })(jQuery);
