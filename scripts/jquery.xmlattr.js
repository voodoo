/* -------------------------------------------------------------------------

jquery.xmlattr.js
version 0.1

Dual licensed under the MIT and GPL licenses.
----------------------------------------------------------------------------
Copyright (C) 2008 Malthe Borch <mborch@gmail.com>
----------------------------------------------------------------------------
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

------------------------------------------------------------------------- */ 

(function($) {
  var known_namespaces = [];
  var reverse_prefix_map = {};
  
  $.fn.xmlattr = function(name) {
    // parse name using James Clark notation, e.g. "{ns}name"
    var params = name.substring(1).split('}');
    if (params.length == 1) return $(this).attr(name);
    var ns = params[0];
    var name = params[1];

    // for each namespace uri, we have to find out what the current
    // prefix assignment is in the document; if the namespace is not
    // known, traverse parent chain until we find the corresponding
    // namespace prefix mapping attribute
    if ($.inArray(ns, known_namespaces) == -1) {
      var items = $(this).parents().andSelf();
      for (var i=0; i<items.length; i++) {
        var attributes = items[i].attributes;
        for (var j=0; j<attributes.length; j++) {
          var p = attributes[j];
          var k = p.name.indexOf("xmlns:");
          if (k == 0) {
            var prefix = p.name.substring(6);
            known_namespaces.push(p.value);
            reverse_prefix_map[p.value] = prefix;
            if (ns == p.value) {
              return $(this).attr(prefix+":"+name);
            }
          }
        }
      }
    } else {
      var prefix = reverse_prefix_map[ns];
      return $(this).attr(prefix+":"+name);
    }
  };
 })(jQuery);
