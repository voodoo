###
# Copyright (c) 2008, Malthe Borch
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircmsgs as ircmsgs
import supybot.ansi as ansi

import threading
import os
import time
import re
import itertools

commit_header_regex = re.compile('^(?P<digest>[a-z0-9]+) (?P<name>[^0-9]+) (?P<date>.+):\s+(?P<msg>.*)')

file_change_regex = re.compile('^:(?:[^A-Z]+) (?P<type>[A-Z])\t(?P<path>.*)')

class Commit(object):
    def __init__(self, digest, name, date, msg):
        self.digest = digest
        self.name = name
        self.date = date
        self.msg = msg
        self.changes = []

class Change(object):
    def __init__(self, _type, path):
        self.type = _type
        self.path = path
        
class Insomniac(threading.Thread):
    """The insomniac tails and pulls the repository."""

    flood_limit = 450
    flood_delay = 30
    
    def __init__(self, git, irc, channel, path):
        super(Insomniac, self).__init__()
        self.git = git
        self.path = path
        self.command = git.registryValue('gitCommand')
        self.active = True
        self.irc = irc
        self.channel = channel
        
        # initialize skip register
        commits = self._get_commits([])
        self.skip = commits.keys()
        
    def run(self):
        while self.active:
            os.chdir(self.path)
            
            os.spawnvp(os.P_WAIT, self.command,
                       ["","pull"])

            commits = self._get_commits(self.skip)
            self.skip.extend(commits.keys())

            # group commits by name
            by_name = {}

            for commit in commits.values():
                # skip merge commits
                if not commit.changes:
                    continue

                name = commit.name

                if name in by_name:
                    by_name[name].append(commit)
                else:
                    by_name[name] = [commit]

            # post commits
            for commits in by_name.values():
                name = commits[0].name
                changes = list(itertools.chain(
                    *[commit.changes for commit in commits]))
                msgs = " || ".join([commit.msg for commit in commits])

                log = u': \'lo and behold---%s (changes in %d files): "%s".' % \
                      (name, len(changes), msgs)

                if len(log) > self.flood_limit:
                    log = log[:self.flood_limit] + '...'

                self.irc.queueMsg(ircmsgs.action(self.channel, log))
	        self.irc.noReply()

                # if by posting the log we've reached the limit
                # lets hold our horses
                if len(log) > self.flood_limit:
                    time.sleep(self.flood_delay)

            for i in range(120):
                if not self.active:
                    return
                
                time.sleep(1)
                
    def _get_commits(self, skip):
        os.chdir(self.path)
        cmd = '%s-log --pretty=format:"%%H %%cn %%ci: %%s" --raw' % self.command
        f = os.popen(cmd, 'r')

        commits = {}
        changes = []

        digest = None
        commit = None
        
        for line in f:
            header = commit_header_regex.match(line)

            if header:
                # save commit
                if commit is not None and not ignore:
                    commits[digest] = commit

                # new commit object
                digest = header.group('digest')
                commit = Commit(digest,
                                header.group('name'),
                                header.group('date'),
                                header.group('msg'))

                ignore = False

                if digest in skip:
                    ignore = True

                continue

            change = file_change_regex.match(line)

            if change:
                commit.changes.append(
                    Change(change.group('type'), change.group('path')))

        return commits

class Git(callbacks.Plugin):

    insomniac = None
    
    def __init__(self, irc):
        super(Git, self).__init__(irc)
        
    def doJoin(self, irc, msg):
        channel = msg.args[0]

        # wait for ourselves to join
        if not ircutils.strEqual(irc.nick, msg.nick):
            return

        path = self.registryValue('repositoryPath', channel)

        if self.insomniac is None:
            self.insomniac = Insomniac(self, irc, channel, path)
            self.insomniac.start()
        
    def die(self):
        if self.insomniac is not None:
            self.insomniac.active = False
        super(Git, self).die()

Class = Git


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
