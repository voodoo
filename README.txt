Vudo
====

         .lOXNNXkc.     
       :0WMMMMMMMMWk,   
     ;NMMW0dod0MMMMMM0. 
    :MMMNl;,,,;dMMMMMMN.
    XMMMo,';;.',0MMMMMMO
    NMMN,'lNMo.'xMMMMMMW
    oMMW;.kWMO.cNMMMMMMX
     lWM0,kMN;lWMMMMMMMc
      .OMKlXlkWMMMMMMM0 
        ,KW0NMMMMMMMMN. 
          oMKdKMMMMMW'  
           .  0MMMMW,     Vudo
              WMMMM:      ----
             'MMMMc   
             lMMMl  
             OMMl     Point and spray
            .WMl       web authoring!
            cMc         
            0:          
           ,,           
           .            

This is the source-code repository for the Vudo project.

Master branch:
   git+ssh://repo.or.cz/srv/git/voodoo.git
   http://repo.or.cz/r/voodoo.git

See summary at:
   http://repo.or.cz/w/voodoo.git


Project description
-------------------
 
The project aims to create reusable library code for web content
programming using Zope-related technology such as ``repoze.bfg``.

Using the source repository
---------------------------
   
We use the ``git`` source control system. To get started:

1. Install git on your system. On a Mac:

   $ sudo port install git-core

2. Create a user account at: http://repo.or.cz

3. Clone the master repository:

   $ git clone git+ssh://<<username>>@repo.or.cz/srv/git/voodoo.git

Hack along. Because we're using git, you can check in changes locally,
in fact, you just have cloned a complete repository to your local
machine.

You'll probably want to read up on the general usage of git before
proceeding with commits or updates. This is a good starting point.
http://git.or.cz/course/svn.html

Contributors
------------

Malthe Borch <mborch@gmail.com>
Stefan Eletzhofer <stefan.eletzhofer@inquant.de>
Christof Haemmerle <reco@nex9.com>

test