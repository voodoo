Overview
========

This package provides WYSIWYG editing of text and live-preview
integration.

Note that the version number postfix follows the release of the
editor.
