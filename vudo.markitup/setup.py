##############################################################################
#
# Copyright (c) 2008 Zope Corporation and Contributors.
# All Rights Reserved.
#
# This software is subject to the provisions of the Zope Public License,
# Version 2.1 (ZPL).  A copy of the ZPL should accompany this distribution.
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#
##############################################################################

from setuptools import setup, find_packages
import sys, os

version = '0.1-1.1.4'

setup(name='vudo.markitup',
      version=version,
      description="MarkItUp! integration.",
      keywords='',
      author='David Brenneman',
      author_email='vudo@googlegroups.com',
      url='',
      license='ZPL',
      include_package_data=True,
      zip_safe=False,
      packages=find_packages('src'),
      package_dir = {'': 'src'},
      namespace_packages=['vudo'],
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      [vudo.skin]
      markitup=vudo.markitup:provide_skin [skin]
      """,
      extras_require={
          "skin": "vudo.skinsetup",
          }
      )
