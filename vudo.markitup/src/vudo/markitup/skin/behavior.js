(function($) {
  $(document).ready(function() {
      var options = {
      onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
      onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
      onTab:    		{keepDefault:false, replaceWith:'    '},
      markupSet:  [
        {name:'Bold', key:'B', openWith:'(!(<strong>|!|<b>)!)', closeWith:'(!(</strong>|!|</b>)!)' },
        {name:'Italic', key:'I', openWith:'(!(<em>|!|<i>)!)', closeWith:'(!(</em>|!|</i>)!)'  },
        {name:'Stroke through', key:'S', openWith:'<del>', closeWith:'</del>' },
        {separator:'---------------' },
        {name:'Picture', key:'P', replaceWith:'<img src="[![Source:!:http://]!]" alt="[![Alternative text]!]" />' },
        {name:'Link', key:'L', openWith:'<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', closeWith:'</a>', placeHolder:'Your text to link...' },
        {separator:'---------------' },
        {name:'Clean', className:'clean', replaceWith:function(markitup) { return markitup.selection.replace(/<(.*?)>/g, "") } },
        {name:'Preview', className:'preview',  call:'preview'}
                   ]
      };

      // enable editor for original document body
      $('.vudo-richtext.markitup-editor').markItUp(options);

      // enable editor when an ajax-request has returned an HTML
      // response
      $(document).ajaxComplete(function(result, request) {
          var ctype = request.getResponseHeader('content-type');
          if (ctype.indexOf('text/html') == 0) {
            $(this).find('.vudo-richtext.markitup-editor').markItUp(options);
          }
        });
    });
 })(jQuery);
