#
def provide_skin():
    from vudo.skinsetup import provide_skin
    return provide_skin(
            package="vudo.markitup",
            name="markitup",
            skin_path="skin")
