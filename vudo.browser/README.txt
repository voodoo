Overview
========

The ``vudo.browser`` implements a client-side interactive resource
browser.

Resources are required to provide a minimal set of metadata, a unique
identifier and a visual representation (thumbnail). An API defines how
components must be set up to allow queries that provide resources.

A typical application of the package is to allow users to query and
select multimedia content, or references to site content.


