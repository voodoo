from zope import interface

import interfaces
import mimetypes
import os

class DirectoryResourceQueryResult(object):
    interface.implements(interfaces.IResourceQueryResult)

    def __init__(self, mimetype, relative_path):
        self.relative_path = relative_path
        self.mimetype = mimetype

    def __repr__(self):
        return '<%s uid="%s">' % (type(self).__name__, self.uid)

    @property
    def uid(self):
        return self.relative_path

class DirectoryResourceProvider(object):
    """This provider indexes a directory (in a very inefficient way),
    and makes it available for full-text queries. It's provided as an
    example of a resource content provider."""

    interface.implements(interfaces.IResourceProvider)

    def __init__(self, path):
        self.path = path

    def query(self, mimetype_filter=None, searchable_text=None, **kwargs):
        results = set()

        if searchable_text is not None:
            for path in self.get_paths():
                filename = os.path.join(self.path, path)
                mimetype, ignored = mimetypes.guess_type(filename)

                # filter on mimetype if required
                if mimetype_filter is not None and mimetype not in mimetype_filter:
                    continue

                if searchable_text in file(filename).read().decode('utf-8'):
                    result = DirectoryResourceQueryResult(mimetype, path)
                    results.add(result)

        return len(results), results

    def get_paths(self):
        """Return relative paths to files found in this directory."""

        assert os.path.exists(self.path)

        for dirpath, dirs, filenames in os.walk(self.path):
            for filename in filenames:
                yield os.path.join(dirpath[len(self.path)+1:], filename)


