import mimetypes
import urllib
import urllib2
import jsonlib
import os

from zope import interface
from vudo.browser import interfaces

class FilecatResourceQueryResult(object):
    interface.implements(interfaces.IResourceQueryResult)

    def __init__(self, mimetype, url, metadata):
        self.url = url
        self.metadata = metadata
        self.mimetype = mimetype

    def __repr__(self):
        return '<%s uid="%s">' % (type(self).__name__, self.uid)

    @property
    def uid(self):
        return self.url

class FilecatResourceProvider(object):
    """Filecat resource provider

    This resource provider delegates queries to a ``repoze.filecat`` compatible
    host.
    """

    interface.implements(interfaces.IResourceProvider)

    def __init__(self, url):
        self.url = url

    def query(self, mimetype_filter=None, searchable_text=None, **kwargs):
        estimated_matches, items = self.query_filecat(searchable_text, **kwargs)
        results = set()

        for item in items:
            if mimetype_filter and item["mimetype"] not in mimetype_filter:
                continue

            result = FilecatResourceQueryResult(url=item["url"],
                    mimetype=item["mimetype"], metadata=item["metadata"])
            results.add(result)

        return estimated_matches, results

    def query_filecat(self, searchable_text, **kwargs):
        """ perform query, return decoded results """

        query = kwargs.copy()
        if searchable_text is not None:
            query["query"] = searchable_text
            
        params = urllib.urlencode(query)
        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Accept": "application/json"}

        request = urllib2.Request("/".join([self.url, "query"]))
        for k, v in headers.items():
            request.add_header(k, v)
            request.add_data(params)
        stream = urllib2.urlopen(request)

        return jsonlib.read(stream.read())

