(function($) {
  $.fn.vudoBrowser = function(query_url, field_name) {
    var target = $(this);
    
    $.getJSON(query_url, function(data) {
        var matches_estimated = data[0];
        var results = data[1];
        
        var list = $("ul", target);
        if (list.length) {
          list.empty();
        } else {
          list = $("<ul />").appendTo(target);
        }

        for (href in results) {
          var item = $("<li />").appendTo(list);
          var link = $("<a />").appendTo(item);
          var field = $("<input type='checkbox' />").appendTo(item);
          
          var value = results[href];
          var label = value['title'] || href;
          
          link.text(label);
          link.attr('href', href);

          field.attr('name', field_name);
          field.attr('value', href);
        }
      });
  };
 })(jQuery);
