(function($) {
  $(document).ready(function(){
      $("a.vudo-browser.browse-link")
        .click(function() {
            var selector = $(this).xmlattr('{http://namespaces.repoze.org/vudo}browser');
            var field = $(this).xmlattr('{http://namespaces.repoze.org/vudo}field');
            var url = $(this).attr('href');

            $(selector).vudoBrowser(url, field);
            return false;
          });
    });
 })(jQuery);
