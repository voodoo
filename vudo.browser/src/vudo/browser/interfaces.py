from zope import interface

from repoze.bfg.interfaces import IRequest

IJSONRequest = IRequest({'HTTP_ACCEPT': 'application/json'})

class IResourceProvider(interface.Interface):
    """Provides resources through a query interface."""

    def query(self, mimetype_filter=None, **kwargs):
        """Returns a tuple of (estimated) matches and a set of
        resource query results.

        If ``mimetype_filter`` is provided, only results that have a
        mimetype in the filter are returned.
        """

class IResourceQueryResult(interface.Interface):
    """A resource query result (brain)."""

    uid = interface.Attribute(
        """Unique identifier for this resource.""")

    mimetype = interface.Attribute(
        """MIME-type specification of the resource.""")

class IResourceQueryTile(interface.Interface):
    """This interface provides properties for a user interface to
    render a visual tile representation of a result query item
    displayed in the browser."""

    thumbnail = interface.Attribute(
        """URL that points to a thumbnail for this resource.""")

    caption = interface.Attribute(
        """Resource caption to be displayed in the user interface.""")

class IQueryResultsSerializer(interface.Interface):
    """Serializes query results to a format compatible with the
    request-type, e.g. a JSON data request."""

    def format(matches_estimated, results):
        """Serializes and formats results to an UTF-8 encoded string."""
