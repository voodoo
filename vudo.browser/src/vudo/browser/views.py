from zope import component
from webob import Response

import interfaces

def query(context, request):
    matches_estimated = 0
    results = set()
    
    for name, utility in component.getUtilitiesFor(interfaces.IResourceProvider):
        matches, provided = utility.query(**dict(request.params.items()))
        matches_estimated += matches
        results = provided.union(results)
        
    return interfaces.IQueryResultsSerializer(request).format(
        matches_estimated, results)

def query_to_response(context, request):
    result = query(context, request)
    return Response(result, content_type='application/json')

