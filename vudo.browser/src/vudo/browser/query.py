from zope import interface
from zope import component

import interfaces
import jsonlib

from repoze.bfg.interfaces import IRequest

class QueryResultsSerializer(object):
    interface.implements(interfaces.IQueryResultsSerializer)

    def __init__(self, request):
        self.request = request

    def normalize(self, results):
        items = {}
        for result in results:
            item = items[result.uid] = {
                'mimetype': result.mimetype}

            # add properties to render tile
            if interfaces.IResourceQueryTile.providedBy(result):
                tile = result
            else:
                tile = interfaces.IResourceQueryTile(result, None)

            if tile is not None:
                item.update({
                    'thumbnail': tile.thumbnail,
                    'caption': tile.caption})

        return items

    def format(self, matches_estimated, results):
        return NotImplementedError("Must be implemented by subclass.")

class JSONQueryResultsSerializer(QueryResultsSerializer):
    component.adapts(interfaces.IJSONRequest)

    def format(self, matches_estimated, results):
        items = self.normalize(results)
        return jsonlib.write((matches_estimated, items))
