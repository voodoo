Developer documentation
=======================

The browser relies on resource providers to make resources available
through queries.

  >>> from vudo.browser.directory import DirectoryResourceProvider
  >>> directory = DirectoryResourceProvider(os.path.join(path, 'static'))

A full-text query:

  >>> estimated_matches, results = \
  ...    directory.query(searchable_text=u"Rabbit-Hole")

  >>> results
  set([<DirectoryResourceQueryResult uid="alice-in-wonderland.txt">])

We can restrict the query to a set of mimetypes:

  >>> estimated_matches, results = \
  ...    directory.query(mimetype_filter=('text/html',), searchable_text=u"Rabbit-Hole")

  >>> results
  set([])

  >>> estimated_matches, results = \
  ...    directory.query(mimetype_filter=('text/plain',), searchable_text=u"Rabbit-Hole")

  >>> results
  set([<DirectoryResourceQueryResult uid="alice-in-wonderland.txt">])

To make the resource provider available to the higher-level browser
query API, we need to register it as a utility.

  >>> component.provideUtility(directory, name="static")

JSON interface
--------------

An HTTP interface which uses the JSON format provides communication
between the client-side application and the server-side application
host.

To examine this interface we'll set up context and request objects.

  >>> from webob import Request
  >>> environ = {'HTTP_ACCEPT': 'application/json'}
  >>> context = None
  >>> request = Request(environ)

Emit notification of the new request.

  >>> from repoze.bfg.events import NewRequest
  >>> from zope.event import notify
  >>> notify(NewRequest(request))

The ``query`` view distributes the query to all resource providers and
gathers the query results before returning them as a JSON data
structure.

  >>> from vudo.browser.views import query
  >>> from jsonlib import read
  >>> read(query(context, request))
  [0L, {}]

Let's add a text query to the request environment, similar to the one
in the previous section.

  >>> request = Request.blank('?searchable_text=Rabbit-Hole', environ)
  >>> notify(NewRequest(request))

We'll now get a Python dict structure representing the query results

  >>> read(query(context, request))
  [1L, {u'alice-in-wonderland.txt': {u'mimetype': u'text/plain'}}]



