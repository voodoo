class ContentList(object):
    """A list of content items. Individual content items may be
    accessed by ``uid`` using dictionary lookup, in addition to
    integer index lookup."""

    def __getitem__(self, key):
        if isinstance(key, int):
            return super(ContentList, self).__getitem__(key)

        for content in self:
            if content.uid == key:
                return content
        raise KeyError(key)
