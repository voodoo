from zope import component

from repoze.bfg import traversal
from repoze.bfg.interfaces import ILocation
from repoze.bfg.interfaces import VH_ROOT_KEY
from repoze.bfg.location import LocationProxy

import interfaces

_marker = object()

class ExtendedModelGraphTraversal(traversal.ModelGraphTraverser):
    """This traverser extends the default model graph traverser with a
    handler to support `++` namespace segments."""
    
    component.adapts(interfaces.IModel)

    def __call__(self, environ, _marker=_marker):
        try:
            path = environ['PATH_INFO']
        except KeyError:
            path = '/'
        try:
            vroot = environ[VH_ROOT_KEY]
            path = vroot + path
        except KeyError:
            pass
            
        path = traversal.traversal_path(path)

        ob = self.root
        name = ''
        locatable = ILocation.providedBy(ob)

        i = 1
        for segment in path:
            if segment[:2] =='@@':
                return ob, segment[2:], list(path[i:])
            if segment.startswith('++') and segment.endswith('++'):
                next = getattr(ob, segment[2:-2])
            else:
                try:
                    getitem = ob.__getitem__
                except AttributeError:
                    return ob, segment, list(path[i:])
                try:
                    next = getitem(segment)
                except KeyError:
                    return ob, segment, list(path[i:])
                if locatable and (not ILocation.providedBy(next)):
                    next = LocationProxy(next, ob, segment)
            ob = next
            i += 1

        return ob, '', []
