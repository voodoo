(function($) {
  $(document).ready(function(){
      // tags editor
      $('.vudo-model.tag-editor').tumblertags();

      var selected = null;
      
      // static tree
      $.each($(".vudo-tree"), function(i, o) {
          var tree = new tree_component();
          var options = {
            rules: {
              clickable: "all",
              creatable: "all",
              deletable: "all",
              renameable: "all",
              draggable: "all",
              dragrules: "all"
            },
            callback: {
            onchange : function(node, tree) { selected = node },
            onrename : function(node, lang, tree) { console.log("Node renamed."); }
            }
          };
          
          tree.init($(o), options);

          $(o).children('input[name="rename"]').click(function() {
              tree.selected = $(selected);
              tree.rename();
              return false;
            });

          // make sure form is not submitted on enter
          $(o).parents("form").keypress(function (evt) {
              if (evt.keyCode == 13) {
                return false;
              }
            });
          
        });
      
      // ajax tree
      $.each($(".vudo-async-tree"), function(i, o) {
          var url = $(o).xmlattr('{http://namespaces.repoze.org/vudo}url');

          // todo
        });
    });
 })(jQuery);
