from zope import component
from zope import interface

from repoze.bfg.interfaces import IRequest
from repoze.bfg.security import ViewPermission

from interfaces import IContent

class AddPermission(ViewPermission):
    component.adapts(IContent, IRequest)

    def __init__(self, context, request):
        super(AddPermission, self).__init__(context, request, "add")

class ModifyPermission(ViewPermission):
    component.adapts(IContent, IRequest)

    def __init__(self, context, request):
        super(ModifyPermission, self).__init__(context, request, "modify")

