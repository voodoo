Overview
========

The ``vudo.compositepage`` packages provides functionality to compose
and render a page which is logically composed by disjoint groups of
content.

