import os
from setuptools import setup, find_packages, Extension

def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

setup(name='vudo.compositepage',
      version = '0.1',
      description='Composite page functionality.',
      long_description=read('README.txt'),
      keywords = "vudo composite page",
      classifiers = [
          'Development Status :: 4 - Beta',
          'Environment :: Web Environment',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: Zope Public License',
          'Programming Language :: Python',
          'Natural Language :: English',
          'Operating System :: OS Independent',
          'Topic :: Internet :: WWW/HTTP',
          'Framework :: Zope3'],
      license='ZPL 2.1',
      packages=find_packages('src'),
      package_dir = {'': 'src'},
      package_data = {
          "vudo.compositepage": [
              "skin/*.zcml",
              "skin/compositepage/*.zcml",
              "skin/compositepage/ajax/*.pt",
              "skin/region/*.pt",
              ]
          },
      namespace_packages=['vudo', ],
      install_requires=['setuptools',
                        'repoze.bfg',
                        'repoze.bfg.skins',
                        'repoze.bfg.httprequest',
                        ],  
      include_package_data = True,
      zip_safe = False,
      entry_points="""
      # -*- Entry points: -*-
      [vudo.skin]
      compositepage=vudo.compositepage:provide_skin [skin]
      """,
      extras_require={
          "skin": "vudo.skinsetup",
      },
      )
