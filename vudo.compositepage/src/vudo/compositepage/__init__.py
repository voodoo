import repoze.bfg.httprequest


def provide_skin():
    import os
    from vudo.skinsetup import provide_skin
    return provide_skin(
            package="vudo.compositepage",
            name="compositepage",
            skin_path="skin")
