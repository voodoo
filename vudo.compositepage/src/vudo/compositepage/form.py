from zope import interface
from zope import component

from vudo.cmf.form import EditForm
from repoze.bfg.interfaces import IRequest

import models

class CompositePageForm(EditForm):
    component.adapts(models.CompositePage, IRequest)

    fields = {
        'title': str,
        'description': str,
        'layout': str,
        'content_mapping': {str: [int]}
        }
