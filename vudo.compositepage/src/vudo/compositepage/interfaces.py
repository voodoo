from repoze.bfg.interfaces import IRequest

IAjaxRequest = IRequest({'http_x_requested_with': 'xmlhttprequest'})
