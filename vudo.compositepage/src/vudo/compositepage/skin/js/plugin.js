(function($) {
  $.fn.vudoCompositePageSortable = function(clipboard) {
    widgets = $(this);

    function fix_up_input_field(item) {
      var input = $(item).children('input');
      var name = $(item).parents("ul").next("a").attr('name');
      
      // when the item is dropped, we must make sure to
      // update the `name` attribute of the input element
      // corresponding to the item, such that it matches
      // the slot container
      input.attr('name', name);
    }
    
    widgets.sortable({
      connectWith: [clipboard],
          opacity: .5,
          out: function(e, ui) {
          clipboard.addClass('sort-target');
        },
          stop: function(e, ui) {
          clipboard.removeClass('sort-target');
          fix_up_input_field(ui.item);
        }
      });

    clipboard
    .sortable({
      connectWith: [widgets],
          opacity: .5,
          stop: function(event, ui) {
          fix_up_input_field(ui.item);
        }
      });
  };
  
  $.fn.vudoAjaxEdit = function(href) {
    console.log("vudo.compositepage: vudoAjaxEdit("+href+")");
    
    var target = $(this);
    
    function prepare_ajax_submit() {
      var form = target.children('form');
      var href = decodeURIComponent(form.attr('action'));
      
      if (href) {
        form.attr('action', decodeURIComponent(href));
      }
      
      form.ajaxForm(function(body) {
          target.html(body);
          prepare_ajax_submit();
        });
    }
    
    $(target)
    .load(href, function(html) {
        prepare_ajax_submit();
      });
    
  };
 })(jQuery);
