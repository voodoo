(function($) {
  $(document).ready(function(){
      // edit
      $(".vudo-link")
        .filter(function() {
            return ($(this).xmlattr('{http://namespaces.repoze.org/vudo}ajax') != undefined);
          })
        .click(function() {
            var selector = $(this).xmlattr('{http://namespaces.repoze.org/vudo}ajax');
            var url = $(this).attr('href');
            $(selector).vudoAjaxEdit(url);
            return false;
          });

      // tabs
      $(".vudo-compositepage")
        .filter(function() {
            return ($(this).xmlattr('{http://namespaces.repoze.org/vudo}tabs') != undefined);
          })
        .tabs();

      $.each($(".vudo-compositepage.widget-list"), function(i, o) {
          var selector = $(o).xmlattr('{http://namespaces.repoze.org/vudo}clipboard');
          var clipboard = $(selector);
          $(o).vudoCompositePageSortable(clipboard);
        });

    });
 })(jQuery);
