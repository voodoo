from zope import interface
from zope import component

from zope.lifecycleevent.interfaces import IObjectModifiedEvent

from repoze.bfg.skins.interfaces import ISkinTemplate
from vudo.cmf.interfaces import IPage

import interfaces

class CompositePage(object):
    """A composite page."""

    interface.implements(IPage)

    title = u""
    description = u""

    def get_content_mapping(self):
        return self.__dict__['content_mapping']

    def set_content_mapping(self, mapping):
        try:
            content_mapping = self.__dict__['content_mapping']
            
            for ordering in content_mapping.values():
                del ordering[:]
            
            content_mapping.update(mapping)
        except KeyError:
            self.__dict__['content_mapping'] = mapping

    content_mapping = property(get_content_mapping, set_content_mapping)

    @property
    def regions(self):
        regions = {}
        for name, ordering in self.content_mapping.items():
            regions[name] = CompositeRegion(self, name)
        return regions            

@component.adapter(CompositePage, IObjectModifiedEvent)
def maintain_content_mapping(page, event):
    content = page.content
    items = set(range(len(content)))

    for ordering in page.content_mapping.values():
        map(items.remove, ordering)

    # add any unassigned items to the default mapping
    try:
        page.content_mapping[""].extend(tuple(items))
    except KeyError:
        raise KeyError(
            "Content mapping must have a default slot (empty string).")
    
class CompositeRegion(object):

    def __init__(self, page, name):
        self.name = name
        self.content = page.content
        self.ordering = page.content_mapping[name]
        
    def __iter__(self):
        return (self.content[i] for i in self.ordering)
