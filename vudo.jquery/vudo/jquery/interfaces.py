from zope import interface

class IClientSideScripting(interface.Interface):
    """Marker interface for requests that require scripting."""
