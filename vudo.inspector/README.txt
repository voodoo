Overview
========

This package implements an object inspection user interface
("inspector") and provides Javascript library functionality to support
form interaction using Javascript data structure (suitable for data
exchange using the JSON serialization formnat).

The inspector window is invoked using Javascript (the implementation
is based on the jQuery library); it's up to the calling application to
set up logic that provide interaction.


WSGI Setup
----------

To enable the inspector for a particular web page, include the
stylesheet and javascript file located in the 'static' directory
within the package in the HTML document.

For convenience, a WSGI middleware is included which automatically
enables HTML documents and serves up the required files.

Below is an example configuration:

  [filter:inspector]
  use = egg:vudo.inspector#make_middleware
  path = /static/inspector

