from setuptools import setup, find_packages

setup(name='vudo.inspector',
      version='0.1',
      license='ZPL',
      description="vudo inspector",
      long_description="",
      keywords='',
      # Get more from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=['Programming Language :: Python',
                   'Environment :: Web Environment',
                   'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
                   'Framework :: Zope3',
                   ],
      packages=find_packages('src'),
      package_dir = {'': 'src'},
      package_data = {
          "vudo.inspector": [
              "skin/*.zcml",
              "skin/*.pt",
              "skin/*.css",
              "skin/*.js",
              "skin/images/*.png",
              "skin/images/*.gif",
              ]
          },
      namespace_packages=['vudo'],
      include_package_data=True,
      zip_safe=True,
      install_requires = [
           'setuptools',
           ],
      entry_points="""
      # -*- Entry points: -*-
      [vudo.skin]
      cmf=vudo.inspector:provide_skin [skin]
      """,
      extras_require={
          "skin": "vudo.skinsetup",
          }
      )
