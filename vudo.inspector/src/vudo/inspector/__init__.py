#

def provide_skin():
    import os
    from vudo.skinsetup import provide_skin
    return provide_skin(
            package="vudo.inspector",
            name="inspector",
            skin_path="skin")
