var vudoUIdisableFade = false;
var inspectorContentLoadedHandlers = new Array();

function inspector_ready(target) {
  jQuery.each(inspectorContentLoadedHandlers, function(i, j) {
      j(jQuery);
    });
};

function inspector_ajax_submit(target) {
  var form = target.children('form');
  var href = decodeURIComponent(form.attr('action'));

  if (href) {
    form.attr('action', decodeURIComponent(href));
  }

  form.ajaxForm(function(body) {
      target.html(body);
      inspector_ready(target);
      inspector_ajax_submit(target);
    });
}

(function($) {
  $(document).ready(function(){
      // load inspector position
      var inspector_position_cookie = $.cookie('inspector_position');

      if (inspector_position_cookie) {
        // unpack width and height
        var parts = inspector_position_cookie.split(':');
        var left = parts[0];
        var top = parts[1];

        $('#vudo-inspector').css('left', left);
        $('#vudo-inspector').css('top', top);
      }
                
      //close inspector window
      vudoUIClosable = '.ui-viewlet-titlebar-close'
      $(vudoUIClosable).click(function(){
        $(this).parent().parent().hide();
      });

      // set opacity when rollout, only if not dragging and resizing
      vudoUIFade = '.ui-viewlet-fade';
      $(vudoUIFade)
        .hover(
          function(){
           if(!vudoUIdisableFade){
             $(this).fadeTo("fast", 1);
           }
          },
          function(){
           if(!vudoUIdisableFade){
             $(this).fadeTo("fast", .6);
           }
          }
        );

      // make css tagged elemets draggable
      vudoUIDraggable = '#vudo-inspector.ui-draggable';
      $(vudoUIDraggable)
        .draggable({
          handle: '.ui-viewlet-title',
          opacity: .2,
          delay: 100,
          zIndex: 1000,
          start: function(ev,ui) { vudoUIdisableFade=true;},
          stop: function() {
              vudoUIdisableFade=false;

              // set cookie with position
              var left = $('#vudo-inspector').css('left');
              var top = $('#vudo-inspector').css('top');
              $.cookie('inspector_position', left+':'+top);
            }
          });
      
      // make css tagged elemets resizable
      // not used atm
      vudoUIResizable = '.ui-resizable';
      $(vudoUIResizable)
        .resizable({ 
          minHeight: 36,
          minWidth: 200, 
          autohide: true,
          handlers: 'all',
          transparent: true,
          preventDefault: true,
          start: function(ev,ui)
            {
              vudoUIdisableFade=true;
            },
          stop: function()
              {
                vudoUIdisableFade=false;
              }
          });
      
      // $("#addWidget").click(function () {
      //   alert("test")
      // })

    });
 })(jQuery);

(function($) {
$(document).ready(function() {
    var form_target = $("#vudo-inspector > form");

    // initialize inpector tabs
    inspectorContentLoadedHandlers.push(function($) {
        $('#vudo-inspector >> ul#inspector-tabs').tabs();
      });

    // initially load up "edit" action in inspector
    form_target
      .load('edit', function(html) {
          inspector_ready(form_target);
          inspector_ajax_submit(form_target);
        });
    
  })})(jQuery);
